var animals = function (animalLover) {
	return function (animal) {
		return function () {
			console.log(animalLover + ' loves ' + animal + 'sssss');
		}
	}
}

// calling method 1

var loversLove = animals('Sourav');
loversLove('dogs');

// calling method 2

animals('Sourav')('dogs')();
animals('SOIEF')('ELEPHANTS')();
