var number = 1;

function increment (arg) {
	number = number + arg;
	return number;
}

console.log(increment(1));
console.log(increment(2)); // sideeffect
console.log(increment(2)); // sideeffect