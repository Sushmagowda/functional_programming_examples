// simple iteration
function iterate (n) {
	if (n == 0) {
		return;
	}
	console.log(n);
	iterate(n - 1); // iterating here
}

iterate(10);

// recursive factorial
var factorial = function(num) {
  console.log('getting factorial(' + JSON.stringify(num) + ')');
  if(num === 1) {
    return 1;
  };
  return num * factorial(num - 1);
};
console.log(factorial(5));

// iterating an object recursively
var obj = {
	user: {
		name: 'X',
		age: '1'
	},
	admin: {
		name: 'admin'
	}
};

function iterateObject (obj, n) {
	if (Object.keys(obj).length === 0 || n === 0) {
		return;
	};

	console.log(obj.user.name);
	console.log(obj.admin.name);
	iterateObject(obj, n - 1);
}

iterateObject(obj, Object.keys(obj).length);