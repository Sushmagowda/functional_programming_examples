function memoize(fn) {
  memoize.cache = {}; // object that caches
  return function() {
    var key = JSON.stringify(arguments);

    if(memoize.cache[key]) { // checking if cache exists
      return memoize.cache[key];
    }
    else {
      var val = fn.apply(this, arguments);
      memoize.cache[key] = val; // caching here  
      return val;
    }
  };
}

// recursive factorial
var factorial = memoize(function(num) {
  if(num === 1) {
    return 1;
  };
  return num * factorial(num - 1);
});

console.log(factorial(6));
console.log(factorial(11));
console.log(factorial(8));

