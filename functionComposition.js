var add = function (a) {	
	return a + 1;
}
var square = function (n) {
	return n * n;
}
// call in any order like this

console.log(add(square(2)));
console.log(square(add(2)));

// or create your own compose and call it in any order like this 
var compose = function (f1, f2) {
	return function (val) {
		return f1(f2(val));
	}
}
console.log(compose(square, add)(2));
